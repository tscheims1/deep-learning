import random
import numpy as np
sizes = [700,10,10]    

biases = []
for y in sizes[1:]:
    biases.append ( np.random.randn(y, 1))

weights = []

for x,y in  zip(sizes[:-1], sizes[1:]):     
    weights.append(np.random.randn(y, x))

print(weights)
