from __future__ import print_function

from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

import tensorflow as tf
from tensorflow.python.ops import gen_nn_ops
"""@ops.RegisterGradient("MaxPoolWithArgmax")
def _MaxPoolWithArgmaxGrad(op, grad, some_other_arg):
  return gen_nn_ops._max_pool_grad(op.inputs[0],
                                   op.outputs[0],
                                   grad,
                                   op.get_attr("ksize"),
                                   op.get_attr("strides"),
                                   padding=op.get_attr("padding"),
                                   data_format='NHWC')


"""
def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
  return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='SAME')

def max_unpool(inp, argmax, argmax_mask, k=2):
    return tf.nn.max_unpool(inp, argmax, argmax_mask, ksize=[1, k, k, 1], strides=[1, k, k, 1], padding="SAME")

def UnPooling2x2ZeroFilled(x):
    # https://github.com/tensorflow/tensorflow/issues/2169
    out = tf.concat([x, tf.zeros_like(x)], 3)
    out = tf.concat([out, tf.zeros_like(out)], 2)

    sh = x.get_shape().as_list()
    if None not in sh[1:]:
        out_size = [-1, sh[1] * 2, sh[2] * 2, sh[3]]
        return tf.reshape(out, out_size)
    else:
        shv = tf.shape(x)
        ret = tf.reshape(out, tf.stack([-1, shv[1] * 2, shv[2] * 2, sh[3]]))
        ret.set_shape([None, None, None, sh[3]])
    return ret
    

# Input layer
x  = tf.placeholder(tf.float32, [None, 784], name='x')
y_ = tf.placeholder(tf.float32, [None, 10],  name='y_')
x_image = tf.reshape(x, [-1, 28, 28, 1])

# Need the batch size for the transpose layers.
batch_size = tf.shape(x)[0]


with tf.name_scope("Conv1") as scope:
# Convolutional layer 1
  W_conv1 = weight_variable([5, 5, 1, 32])
  b_conv1 = bias_variable([32])

  h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
  h_pool1 = max_pool_2x2(h_conv1)

# Convolutional layer 2
W_conv2 = weight_variable([5, 5, 32, 64])
b_conv2 = bias_variable([64])

h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
h_pool2 = max_pool_2x2(h_conv2)

# Fully connected layer 1
h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])

W_fc1 = weight_variable([7 * 7 * 64, 1024])
b_fc1 = bias_variable([1024])

h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

# Dropout
keep_prob  = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

# Fully connected layer 2 (Output layer)
W_fc2 = weight_variable([1024, 10])
b_fc2 = bias_variable([10])

y = tf.nn.softmax(tf.matmul(h_fc1_drop, W_fc2) + b_fc2, name='y')

# Evaluation functions
cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))


###deconvolution part
#conv Layer 1
W_conv7 = weight_variable([5, 5, 1, 32])
b_conv7 = bias_variable([1])
deconv_shape_conv7 = tf.stack([batch_size, 28, 28, 1])
h_conv7 = tf.nn.relu(tf.nn.conv2d_transpose(h_conv1, W_conv7, output_shape = deconv_shape_conv7, strides=[1,1,1,1], padding='SAME') + b_conv7)

#conv layer 1 + maxpooling
W_conv7p = weight_variable([5, 5, 1, 32])
b_conv7p = bias_variable([1])
deconv_shape_conv7p = tf.stack([batch_size, 28, 28, 1])
h_pool7 = UnPooling2x2ZeroFilled(h_pool1)
img_pool_layer1 = tf.nn.relu(tf.nn.conv2d_transpose(h_pool7, W_conv7p, output_shape = deconv_shape_conv7p, strides=[1,1,1,1], padding='SAME') + b_conv7p)


#conv layer 2 + max pooling + conv layer 1
W_conv8 = weight_variable([5, 5, 32, 64])
b_conv8 = bias_variable([32])
deconv_shape_conv8 = tf.stack([batch_size, 14, 14, 32])

W_conv8_7 = weight_variable([5, 5, 1, 32])
b_conv8_7 = bias_variable([1])
deconv_shape_conv8_7 = tf.stack([batch_size, 28, 28, 1])

h_conv8 = tf.nn.relu(tf.nn.conv2d_transpose(h_conv2, W_conv8, output_shape = deconv_shape_conv8, strides=[1,1,1,1], padding='SAME') + b_conv8)
h_pool8 = UnPooling2x2ZeroFilled(h_conv8)
img_layer2 = tf.nn.relu(tf.nn.conv2d_transpose(h_pool8, W_conv8_7, output_shape = deconv_shape_conv8_7, strides=[1,1,1,1], padding='SAME') + b_conv8_7)




#####################3


with tf.name_scope('loss'):
    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
    tf.summary.scalar('train_loss', cross_entropy)
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name='accuracy')

# Training algorithm
train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)

# Training steps



##############

with tf.name_scope('Visualize_filters') as scope:

# In this section, we visualize the filters of the first convolutional layers
# We concatenate the filters into one image
# Credits for the inspiration go to Martin Gorner
  W1_a = W_conv1                       # [5, 5, 1, 32]
  W1pad= tf.zeros([5, 5, 1, 1])        # [5, 5, 1, 4]  - four zero kernels for padding
  # We have a 6 by 6 grid of kernepl visualizations. yet we only have 32 filters
  # Therefore, we concatenate 4 empty filters
  W1_b = tf.concat([W1_a, W1pad, W1pad, W1pad, W1pad],3)   # [5, 5, 1, 36]  
  #W1_c = tf.split(3, 36, W1_b)         # 36 x [5, 5, 1, 1]
  W1_c = tf.split(W1_b,36,3)
  W1_row0 = tf.concat(W1_c[0:6],0)    # [30, 5, 1, 1]
  W1_row1 = tf.concat(W1_c[6:12],0)   # [30, 5, 1, 1]
  W1_row2 = tf.concat(W1_c[12:18],0)  # [30, 5, 1, 1]
  W1_row3 = tf.concat(W1_c[18:24],0)  # [30, 5, 1, 1]
  W1_row4 = tf.concat(W1_c[24:30],0)  # [30, 5, 1, 1]
  W1_row5 = tf.concat(W1_c[30:36],0)  # [30, 5, 1, 1]
  W1_d = tf.concat([W1_row0, W1_row1, W1_row2, W1_row3, W1_row4, W1_row5],1) # [30, 30, 1, 1]
  W1_e = tf.reshape(W1_d, [1, 30, 30, 1])
  Wtag = tf.placeholder(tf.string, None)
  #tf.summary.image("Visualize_kernels", W1_e)


########
#gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.3333)
#sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
config = tf.ConfigProto(
        device_count = {'GPU': 0})




#print (tf.shape(W1_e))
tf.summary.image("visualize Kernels",W1_e,max_outputs=100)
#tf.summary.image("testimage_1",mnist.test.images[1]);
#tf.Print(mnist.test.images,[mnist.test.images]) 

## Visualize Input Image
img_1 = tf.reshape(mnist.test.images[0],[28,28])
img_1 = tf.expand_dims(img_1,0);
img_1 = tf.expand_dims(img_1,3);
tf.summary.image("image_1",img_1)

#visualize conv1
tf.summary.image("deconv conv1",h_conv7,3)
tf.summary.image("deconv pool1",img_pool_layer1,3)
tf.summary.image("devcon conv2",img_layer2,3)


print(img_1.shape)
merged = tf.summary.merge_all()
with tf.Session(config=config) as sess:
    
  #merged = tf.summary.merge_all()
  train_writer = tf.summary.FileWriter('/tmp/train',
                                        sess.graph)
  #test_writer = tf.summary.FileWriter('tmp/test')
  #tf.global_variables_initializer().run()  
    
  #summary_writer = tf.summary.FileWriter('/tmp/logs', sess.graph_def)
  sess.run(tf.initialize_all_variables())

  max_steps = 1000
  for step in range(max_steps):
    batch_xs, batch_ys = mnist.train.next_batch(50)
    
    if (step % 100) == 0:
      acc,m_sum,_conv1 = sess.run([accuracy,merged,W_conv1],feed_dict={x: mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0})
      print(step,acc)
      
      
    _,m_sum = sess.run([train_step,merged],feed_dict={x: batch_xs, y_: batch_ys, keep_prob: 0.5})
    train_writer.add_summary(m_sum, step)
    
print(max_steps, sess.run(accuracy, feed_dict={x: mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0}))
