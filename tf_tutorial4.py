import tensorflow as tf
import numpy as np

from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
  return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

with tf.name_scope('conv1_weigths_biases'):
    W_conv1 = weight_variable([5, 5, 1, 32])
    b_conv1 = bias_variable([32])

    tf.summary.histogram('conv1_weights', W_conv1)
    tf.summary.histogram('conv1_biases', b_conv1)

with tf.name_scope('conv2_weigths_biases'):
    W_conv2 = weight_variable([5, 5, 32, 64])
    b_conv2 = bias_variable([64])

    tf.summary.histogram('conv2_weights', W_conv2)
    tf.summary.histogram('conv2_biases', b_conv2)

with tf.name_scope('fc1_weigths_biases'):
    W_fc1 = weight_variable([7 * 7 * 64, 1024])
    b_fc1 = bias_variable([1024])

    tf.summary.histogram('fc1_weigths', W_fc1)
    tf.summary.histogram('fc1_biases', b_fc1)

with tf.name_scope('fc2_weigths_biases'):
    W_fc2 = weight_variable([1024, 10])
    b_fc2 = bias_variable([10])


    tf.summary.histogram('fc2_weigths', W_fc2)
    tf.summary.histogram('fc2_biases', b_fc2)

with tf.name_scope('train_images'):
    x = tf.placeholder(tf.float32, [None, 784]) # input image
    x_image = tf.reshape(x, [-1, 28, 28, 1])



with tf.name_scope('train_labels'):
    y = tf.placeholder(tf.float32, [None, 10])  # input label

with tf.name_scope('dropout_keep_probability'):
    keep_prob = tf.placeholder(tf.float32)

with tf.name_scope('convolution_model'):
    h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
    h_pool1 = max_pool_2x2(h_conv1)

    h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
    h_pool2 = max_pool_2x2(h_conv2)

with tf.name_scope('fully_connected_model'):
    h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
    h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)
    h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)


with tf.name_scope('output_layer'):
    prediction = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

with tf.name_scope('loss'):
    cross_entropy = tf.reduce_mean(-tf.reduce_sum(y * tf.log(prediction), reduction_indices=[1]))
    #cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(prediction, y))
    tf.summary.scalar('train_loss', cross_entropy)

with tf.name_scope('Optimizer'):
    train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)

with tf.name_scope('Accuracy'):
    correct_prediction = tf.equal(tf.argmax(prediction,1), tf.argmax(y,1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

sess = tf.Session()

train_writer = tf.summary.FileWriter("logs/train", sess.graph)
test_writer = tf.summary.FileWriter("logs/test", sess.graph)

merged = tf.summary.merge_all()

config = tf.ConfigProto(
        device_count = {'GPU': 0})

with tf.Session(config=config) as sess:

    tf.initialize_all_variables().run()

    for i in range(6000):

      batch = mnist.train.next_batch(50)
      train_accuracy, _ , loss= sess.run([accuracy, train_step, cross_entropy], feed_dict={x: batch[0], y: batch[1], keep_prob: 0.5})
      #print("step %d, training loss %g" % (i, loss))

      if i%100 == 0:

        batch = mnist.train.next_batch(100)
        train_accuracy, result_train = sess.run([accuracy,merged], feed_dict={x: batch[0], y: batch[1], keep_prob: 1})
        test_accuracy, result_test = sess.run([accuracy, merged], feed_dict={x: mnist.test.images, y: mnist.test.labels, keep_prob: 1})
        train_writer.add_summary(result_train, i)
        test_writer.add_summary(result_test, i)

        print("step %d, training accuracy %g"%(i, train_accuracy))
        print("step %d, test accuracy %g" % (i, test_accuracy))
